================
 soundcloud2mp3
================

Description
===========
This shell script is able to download public and private songs from soundcloud.

Dependencies
============
CUrl & WGet

Installation 
============
Just copy the ``sc2mp3`` file to ``/home/yourname/bin/`` and run it. 

Usage
=====
#. Copy the url of the song's page (format: ``http://soundcloud.com/username/songname``)
#. Open a terminal
#. Navigate to the directory where the .mp3 file should be placed
#. Run ``sc2mp3 "songurl"`` (replace ``songurl`` with the url copied in step 1)
#. Be happy :)

Disclaimer
==========
| This script is a prove of concept and was not made to steal copyrighted songs.
| Please support the artists by buying their music instead.
| All users are responsible for their own actions.

*and so on, and so on...*
